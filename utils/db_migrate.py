# -*- coding:utf-8 -*-


import os
import glob
import argparse
import pyramid.paster

from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker
import sqlalchemy.exc

import logging


MIGRATIONS_TABLE_NAME = 'migrations'

MIGRATIONS_TABLE_CREATE = """
    CREATE TABLE IF NOT EXISTS {tbl_name} (
        id text PRIMARY KEY,
        applied timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now())
    );
""".format(tbl_name=MIGRATIONS_TABLE_NAME)


def main(settings):
    log = logging.getLogger(__name__)
    db_session = sessionmaker(bind = engine_from_config(settings, 'sqlalchemy.'))()

    db_session.execute(MIGRATIONS_TABLE_CREATE)
    db_session.commit()

    migrations_dir = settings['db_migrate.dir']
    os.path.exists(migrations_dir)
    all_files = set(glob.glob(migrations_dir + '/*.sql'))
    applied_migrations = set(id for (id,) in db_session.execute('SELECT id FROM {tbl_name}'.format(tbl_name=MIGRATIONS_TABLE_NAME)))

    new_migrations = all_files - applied_migrations
    if new_migrations:
        for filename in new_migrations:
            log.info('Start migration: %s', filename)
            with open(filename) as f:
                code = f.read().decode('utf-8')
                log.info('Migration content: %s', code)
                db_session.execute(code)
                db_session.execute('INSERT INTO {tbl_name} (id) VALUES (:id)'.format(tbl_name=MIGRATIONS_TABLE_NAME), {
                    'id': filename,
                })
        db_session.commit()
        log.info('Allright! Applied %s migrations', len(new_migrations))



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('config')
    args = parser.parse_args()
    pyramid.paster.setup_logging(args.config)
    settings = pyramid.paster.get_appsettings(args.config)
    main(settings)
