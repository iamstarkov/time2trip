# -*- coding:utf-8 -*-

from wtforms import Form
from wtforms import fields
from wtforms import validators
from wtforms import widgets

from string import strip
from markupsafe import escape

from time2trip.db import db_session, models

stringFilters = (unicode, escape, strip)


######################################
# Custom forms
######################################

class ParentForm(Form):

    @property
    def errors(self):
        errors_dict = {}
        for fieldname, field in self._fields.viewitems():
            field_errors = field.errors

            if isinstance(field_errors, list):
                errors_dict[fieldname] = field_errors

            elif isinstance(field_errors, dict):
                for subfield_name, subfield_errors in field_errors.viewitems():
                    errors_dict[subfield_errors]

        if self._errors is None:
            self.__errors = {
                'success': False,
                'errors': errors_dict
            }
        return self.__errors


######################################
# Custom validators
######################################

class UserPasswordValidator(object):
    def __call__(self, form, field):
        user = db_session.query(models.User).filter_by(email=form.data['email']).first()

        if not user:
            raise validators.ValidationError('User with email: {0} not found'.format(form.data['email']))

        if not user.check_password(password=field.data):
            raise validators.ValidationError('Incorrect password')

class UserIsActiveValidator(object):
    def __call__(self, form, field):
        user = db_session.query(models.User).filter_by(email=form.data['email']).first()
        if not user:
            raise validators.ValidationError('User with email: {0} not found'.format(form.data['email']))

        if not user.is_active:
            raise validators.ValidationError('User is disabled!')

class DatabaseRowUniqueValidator(object):
    def __init__(self, model):
        self.model = model

    def __call__(self, form, field):
        model_field = getattr(self.model, field.name)
        obj = (db_session.query(self.model).filter(model_field == field.data)).first()
        if obj:
            raise validators.ValidationError('Exists already')


class DatabaseRowExistsValidator(object):
    def __init__(self, model, column_name=None):
        self.model = model
        self.column_name = column_name

    def __call__(self, form, field):
        model_field = getattr(self.model, self.column_name or field.name)
        obj = (db_session.query(self.model).filter(model_field == field.data)).first()

        if not obj:
            raise validators.ValidationError('Does not exist')


######################################
# Forms
######################################

class AuthReg(ParentForm):
    email = fields.StringField(
        default='',
        filters=[strip],
        validators=[
            validators.DataRequired(),
            validators.Email(),
            DatabaseRowUniqueValidator(models.User)
        ]
    )
    name = fields.StringField(default='', filters=[strip], validators=[validators.DataRequired(), validators.Length(min=3, max=30)])
    password = fields.PasswordField(validators=[validators.DataRequired(), validators.Length(min=3, max=255)])
    confirm_password = fields.PasswordField(validators=[validators.DataRequired(), validators.EqualTo('password')])


class AuthLogin(ParentForm):
    email = fields.StringField(
        default='',
        filters=[strip],
        validators=[
            validators.DataRequired(),
            validators.Email(),
            DatabaseRowExistsValidator(models.User),
            UserIsActiveValidator()]
    )

    password = fields.PasswordField(
        validators=[
            validators.DataRequired(),
            validators.Length(max=255),
            UserPasswordValidator()]
    )

class NewRoute(ParentForm):
    title = fields.StringField(default='', filters=[strip], validators=[validators.DataRequired(), validators.Length(min=3, max=254)])
    description = fields.StringField(default='', widget=widgets.TextArea(), validators=[validators.Length(max=400)])


######################################
# SPA Customs
######################################

class CheckRoutePermission(object):
    def __call__(self, form, field):
        user = db_session.query(models.User).filter_by(id=field.data).first()
        if user and user.is_active:
            if user.is_admin:
                return
            route = db_session.query(models.Route).filter(models.Route.id == form.route_id.data).first()
            if route:
                if route.get_user_role(user) not in form.permissions:
                    raise validators.ValidationError('Access denied!')
            else:
                raise validators.ValidationError('Invalid route_id!')
        else:
            raise validators.ValidationError('Invalid user!')

class PermissionRequired(ParentForm):
    permissions = ['creator']

    route_id = fields.IntegerField(validators=[validators.DataRequired()])
    user_id = fields.IntegerField(
        validators=[
            validators.DataRequired(),
            CheckRoutePermission()
        ])




######################################
# SPA Schemas
######################################

class Authentication(ParentForm):
    route_id = fields.IntegerField(validators = [validators.DataRequired(message = 'required')])
    token = fields.IntegerField(validators = [validators.DataRequired(message = 'required')])


class Route(PermissionRequired):
    permissions = ['creator', 'member', 'viewer']
    id = fields.IntegerField(validators=[validators.DataRequired()])

class NewPoint(PermissionRequired):
    permissions = ['creator', 'member']

    lat = fields.FloatField(validators=[validators.DataRequired()])
    lng = fields.FloatField(validators=[validators.DataRequired()])
    title = fields.StringField(validators=[validators.DataRequired()])

class Point(PermissionRequired):
    permissions = ['creator', 'member', 'viewer']
    id = fields.IntegerField(validators=[validators.DataRequired()])

class DeletePoint(Point):
    permissions = ['creator', 'member']
