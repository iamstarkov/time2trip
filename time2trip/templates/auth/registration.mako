<%namespace name="defs" file="time2trip:templates/defs.mako"/>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="${request.static_url('time2trip:static/images/favicon.ico')}" type="image/x-icon">

    <title>time2trip</title>
    <script src="${request.static_url('time2trip:static/plugins/jquery.min.js')}"></script>
    <link href="${request.static_url('time2trip:static/plugins/bootstrap/css/bootstrap.css')}" rel="stylesheet">
    <link href="${request.static_url('time2trip:static/css/styles.css')}" rel="stylesheet">
  </head>

  <body>
    <div class="navbar navbar-custom navbar-fixed-top">
      <div class="navbar-header">
          <a class="navbar-brand" href="${request.route_url('home')}"><img src="${request.static_url('time2trip:static/images/logo2_white.png')}" style="margin-top: -5px; margin-right: 10px;">time2trip</a>
      </div>
    </div>
    <div class="container" style="margin-top: 15px">

      <div class="auth-form">
        <div class="language">
          %if request.locale_name == 'en':
              <a href="/locale?language=ru"><img src="${request.static_url('time2trip:static/images/flags/32/ru.png')}"> </a>
              <img src="${request.static_url('time2trip:static/images/flags/32/gb.png')}" style="opacity: 0.4">
          %elif request.locale_name == 'ru':
              <img src="${request.static_url('time2trip:static/images/flags/32/ru.png')}" style="opacity: 0.4">
              <a href="/locale?language=en"><img src="${request.static_url('time2trip:static/images/flags/32/gb.png')}"></a>
          %endif
        </div>
        <div class="auth-menu">
          <a href="${request.route_url('login')}">${_("Login")}</a> / ${_("Create account")}
        </div>


        <form class="form-signin" role="form" method="post">
          ${defs.field_with_errors(form.email, class_="form-control", autofocus="", required="", placeholder="Email")}
          ${defs.field_with_errors(form.name, class_="form-control", autofocus="", required="", placeholder=_("Name"))}
          ${defs.field_with_errors(form.password, class_="form-control", autofocus="", required="", placeholder=_("Password"))}
          ${defs.field_with_errors(form.confirm_password, class_="form-control", autofocus="", required="", placeholder=_("Confirm password"))}
          <button class="btn btn-lg btn-primary btn-block" type="submit">${_('Sign in')}</button>
            ## <a style="display:block; text-align:center" href="${request.route_url('auth.reset_password')}">${_("Forget password?")}</a>
        </form>
      </div>

    </div>
  </body>
</html>
