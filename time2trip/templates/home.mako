<%inherit file="time2trip:templates/base.mako"/>
<%namespace name="defs" file="time2trip:templates/defs.mako"/>

<%block name="title">${_("My routes")}</%block>

<%block name="styles">
  <link href="${request.static_url('time2trip:static/css/styles.css')}" rel="stylesheet">
</%block>

<%block name="scripts">
  ## <script src="${request.static_url('time2trip:static/js/compiled/utils.js')}"></script>
  <script src="${request.static_url('time2trip:static/js/compiled/home.js')}"></script>
</%block>

<%block name="body">
  <div class="container-fluid" style="padding-top: 15px; height:100%; background: #eee;">
    <div class="row" style="max-width: 800px; background: white; position: relative; margin: 0 auto; box-shadow: 0 3px 6px 1px rgba(0, 0, 0, 0.16); height: 100%;">
      <div class = "col-md-12 col-lg-12">
        <div class="row">
          %if not request.user.email:
            <div class="custom-note" style="margin-top:15px;"> You don't have email</div>
          %else:
            %if not request.user.is_email_confirmed:
              <div class="custom-note primary" style="margin-top:15px;"> Your email is not confirmed. Check you email. If you dont receive activation email go to profile</div>
            %endif
            
          %endif
          <div class="col-md-12 col-lg-12" style="padding-top:15px;">
            <div class="active-routes">
              <div class="active-routes-head">
                <div class="pull-left"><h4>${_('My routes')} (${len(routes)})</h4></div>
                <div class="pull-right">
                  <button type="button"
                      class="btn ${'btn-default' if not form.errors['errors'] else 'btn-primary'}"
                      onclick="$('.new-route-form').slideToggle();"><span
                      class="hidden-xs">${_('New route')}</span> <i class="fa fa-plus"> </i>
                  </button>
                </div>
              </div>
              <div class="active-routes-body">
                <div class="new-route-form" ${"style=display:none" if not form.errors['errors'] else ''}>
                  <form role="form" method="post" action="${request.route_url('home')}">
                    <div class="row">
                      <div class="col-md-6">
                        ${defs.field_with_errors(form.title, class_="form-control", placeholder=_('Title'), style="margin-bottom: 10px")}
                        ${defs.field_with_errors(form.description, class_="form-control", placeholder=_('Description'), rows=3, style="margin-bottom: 10px; resize:none")}
                      </div>
                      <div class="col-md-6">
                        <select class="form-control">
                          <option selected disabled>${_('Choose type of your trip')}</option>
                          %for type in route_types:
                            <option value="${type.id}">${type.localize_col('name', request.locale_name)}</option>
                          %endfor
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12" style="text-align: center">
                        <button type="submit" class="btn btn-primary">${_('Create route')}</button>
                      </div>
                    </div>
                  </form>
                </div>
                %if routes:
                  <div class="panel-group" id="routes">
                    % for route in routes:
                      ${defs.route(route)}
                    % endfor
                  </div>
                %else:
                  <div class="no-routes">
                    ## <h4>${_('No active routes')}</h4>
                    <div class="no-object">
                      <div class="icon">
                        <i class="fa fa-map-o"></i>
                      </div>
                      <div class="text">
                        You don't have any routes
                      </div>
                    </div>
                  </div>

                %endif
              </div>
              ## %if shared_routes:
              ##   <div class="shared-routes">
              ##     <div class="shared-routes-head">
              ##       <div class="pull-left"><h4>${_("Shared routes")} (${len(shared_routes)})</h4></div>
              ##     </div>
              ##     <div class="shared-routes-body">
              ##       <div class="panel-group" id="shared_routes">
              ##           % for route in shared_routes:
              ##             ${defs.route(route, type='shared_route')}
              ##           % endfor
              ##       </div>
              ##     </div>
              ##   </div>
              ## %endif

            </div>
          </div>
        </div>

      </div>
      
    </div>

  </div>
</%block>