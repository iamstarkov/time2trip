<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <%block name="meta"></%block>

        <title><%block name="title"></%block></title>
        
        <link rel="shortcut icon" href="${request.static_url('time2trip:static/images/favicon.ico')}" type="image/x-icon">
        <link href="${request.static_url('time2trip:static/plugins/bootstrap/css/bootstrap.css')}" rel="stylesheet">
        <link href="${request.static_url('time2trip:static/css/styles.css')}" rel="stylesheet">
        <%block name="styles"></%block>

        <script src="${request.static_url('time2trip:static/plugins/jquery.min.js')}"></script>
        <script src="${request.static_url('time2trip:static/plugins/bootstrap/js/bootstrap.min.js')}"></script>
        <script src="${request.static_url('time2trip:static/plugins/noty/packaged/jquery.noty.packaged.min.js')}"></script>
        <script src="${request.static_url('time2trip:static/plugins/noty/themes/bootstrap.js')}"></script>
        <script src="${request.static_url('time2trip:static/js/utils.js')}"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                path = window.location.href;
                $('.nav li a[href="${request.current_route_url()}"]').parent().addClass('active')
                % for m in request.session.pop_flash():
                    show_notify("${m}")
                % endfor
            });
        </script>
        <%block name="scripts"></%block>

    </head>
    <body>
<div class="navbar navbar-custom navbar-fixed-top">
    <div class="navbar-header">
        <div class="navbar-brand">
            <a href="${request.route_url('home')}"><img src="${request.static_url('time2trip:static/images/logo2_white.png')}" style="margin-top: -5px; margin-right: 10px;"><span class="hidden-xs">time2trip</span></a>
        </div>
            <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </div>
    
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
            %if request.user:
                <li><button class="btn btn-default btn-sm" style="margin-top: 10px; margin-right: 50px; color: red" data-toggle="modal" data-target="#feedback_modal"> ${_("Feedback")}</button></li>
                <li>
                    %if request.locale_name == 'en':
                        <a href="${request.route_url('locale', _query={'language':'ru'})}"><img src="${request.static_url('time2trip:static/images/flags/16/ru.png')}"></a>
                    %elif request.locale_name == 'ru':
                        <a href="${request.route_url('locale', _query={'language':'en'})}"><img src="${request.static_url('time2trip:static/images/flags/16/gb.png')}"></a>
                    %endif
                </li>
                <li><a href="${request.route_url('home')}">${_('Home')}</a></li>
                %if request.matched_route.name.startswith('route'):
                    <li class="active"><a href="#">${_('Route')}</a></li>
                %endif
                <li><a href="#">${_('Help')}</a></li>

                %if request.user.is_admin:
                    <li class="${'active' if request.matched_route.name.startswith('admin') else ''}"><a href="#">${_('Admin')}</a></li>
                %endif

                  <li class="dropdown ${'active' if request.matched_route.name in ['profile'] else ''}">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>${request.user.name}</span> <b class="caret"></b></a>
                      <span class="dropdown-arrow"></span>
                      <ul class="dropdown-menu">
                          <li><a href="#">${_('Profile')}</a></li>
                          <li><a href="${request.route_url('logout')}">${_('Logout')}</a></li>
                      </ul>
                  </li>

            % else:
              <li class="divider-vertical"></li>
              <li><a href="${request.route_url('login')}">Login</a></li>
              <li><a href="${request.route_url('registration')}">Registration</a></li>
            % endif
        </ul>

    </div>
</div>

${next.body()}

</body>
</html>