# -*- coding:utf-8 -*-

import datetime

from pyramid.view import view_config, forbidden_view_config
from pyramid.httpexceptions import HTTPFound, HTTPBadRequest
from pyramid.security import remember, forget

import time2trip.forms as forms
from time2trip.db import db_session, models
from time2trip.helpers import generate_uuid

@view_config(route_name='login', renderer='../templates/auth/login.mako')
@forbidden_view_config(renderer='../templates/auth/login.mako')
def login_page(request):
    if request.authenticated_userid:
        return HTTPFound(location=request.route_url('home'))
    login_url = request.route_url('login')
    referrer = request.referer
    if referrer == login_url:
        referrer = '/'
    came_from = request.params.get('came_from', referrer)

    login_form = forms.AuthLogin(request.POST)

    if request.method == 'POST' and login_form.validate():
        user = db_session.query(models.User).filter_by(email=login_form.email.data).one()
        user.last_login = datetime.datetime.utcnow()
        db_session.add(user)
        headers = remember(request, user.id)
        return HTTPFound(location=came_from, headers=headers)

    return {'login_form': login_form}


@view_config(route_name='registration', renderer="../templates/auth/registration.mako")
def registration_page(request):

    if request.authenticated_userid:
        return HTTPFound(location=request.route_url('home'))

    sent_confirmation_link = False
    form = forms.AuthReg(request.POST)
    if request.method == 'POST' and form.validate():

        new_user = models.User()
        new_user.email = form.email.data
        new_user.name = form.name.data
        new_user.options = {'language':request.locale_name}
        new_user.social_networks = {}
        new_user.set_password(form.password.data)
        token = generate_uuid()
        new_user.confirmation_token = token
        new_user.ws_token = generate_uuid()
        new_user.is_active = True
        new_user.is_sent_confirmation_email = True
        db_session.add(new_user)
        db_session.flush()
        db_session.refresh(new_user)

        # mailer = request.registry['mailer']
        # _ = request.translate
        # mailer.add_message(Mail(
        #     to=new_user.email,
        #     subject=_("time2trip. Activate your account.").encode('utf-8'),
        #     html=const.HTML_BODY_CONFIRMATION_MAIL.format(
        #         _("Confirmation link").encode('utf-8'),
        #         request.route_url('auth.confirm', token=token))
        # ))

        # log.info("Registration user. user_id={}".format(new_user.id))

        sent_confirmation_link = True
        header = remember(request, new_user.id)
        return HTTPFound(location=request.route_url('home', headers=header))

    return {'form': form, "sent_confirmation_link": sent_confirmation_link}


@view_config(route_name='logout')
def logout(request):
    headers = forget(request)
    url = request.route_url('home')
    return HTTPFound(location=url, headers=headers)