# -*- coding:utf-8 -*-

import copy

from pyramid.view import view_config
from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound

from time2trip import constants
from time2trip.db import db_session, models
from time2trip import forms


@view_config(route_name='home', renderer='../templates/home.mako', permission='user')
def home_page(request):
    form = forms.NewRoute(request.POST)
    form_error = False
    user = request.user
    if request.method == 'POST':
        print form.data
        if form.validate():
            new_route = models.Route()
            new_route.user_id = user.id
            new_route.title = form.title.data
            db_session.add(new_route)
            db_session.flush()
            db_session.refresh(new_route)

            user__route = models.UserRoute(
                user = user,
                route = new_route,
                permission = constants.UserRole.CREATOR
            )
            db_session.add(user__route)

            url = request.route_url('route', route_id=new_route.id)
            return HTTPFound(location=url)
        else:
            form_error = True

    routes = db_session.query(models.Route).filter_by(user_id=user.id).order_by(models.Route.id).all()
    # route_types = db_session.query(models.RouteType).order_by(models.RouteType.id)
    route_types = []

    return {'routes': routes,
            'form_error': form_error,
            'form': form,
            'user': user,
            'route_types': route_types,
    }



@view_config(route_name='locale')
def set_locale_cookie(request):
    if request.GET['language']:
        language = request.GET['language']
        if language in ('en', 'ru'):
            response = Response()
            response.set_cookie(constants.LOCALE_COOKIE_NAME, value=language, max_age=31536000)  # max_age = year

            if request.authenticated_userid:
                user_options = copy.copy(request.user.options)
                user_options['language'] = language
                request.user.options = user_options
                db_session.add(request.user)
        else:
            response = Response()

    return HTTPFound(location=request.environ.get('HTTP_REFERER') or '/',
                     headers=response.headers)
