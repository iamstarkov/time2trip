window.store = new Vuex.Store ({
    state: {
        route: {},
        route_loaded: false,
        screen_spinner_active: false,
        app_spinner_active: false,
        refbooks: {},
        user: {},
        current_point: {},
        current_subpoint: {}
        map_markers: []
    }


    mutations: {

        set: (state, data) ->
            _.assignIn(state, data)

        choose_point: (state, point_id) ->
            point = state.route.points.filter( (p) -> p.id == point_id)
            if point
                state.current_point = point[0]

        edit_route: (state, route_obj) ->
            _.assignIn(state.route, route_obj)

        add_point: (state, point_obj) ->
            state.route.points.push(point_obj)

        delete_point: (state, point_id, external = false) ->
            point = state.route.points.filter( (p) -> p.id == point_id)
            if point
                point = point[0]
                if point.id == state.current_point.id
                    state.current_point = {}
                state.route.points.splice(state.route.points.indexOf(point), 1)
    }


    actions: {
        edit_route: (context, route_data) ->
            d = Q.defer()
            window.ws.request('edit_route', route_data).then(
                (response) ->
                    if response.success
                        context.commit('edit_route', response.result.route)
                    d.resolve(response.result.route)
            ).fail(
                (err) ->
                    console.log err
            )
            return d.promise

        add_point: (context, point_data) ->
            d = Q.defer()
            # context('set', {})
            window.ws.request('add_point', point_data).then(
                (response) ->
                    if response.success
                        context.commit('add_point', response.result.point)
                        d.resolve(response.result.point.id)
            ).fail(
                (err) ->
                    console.log err
            )
            return d.promise

        delete_point: (context, point_id) ->
            d = Q.defer()
            window.ws.request('delete_point', {'id': point_id}).then(
                (response) ->
                    if response.success
                        context.commit('delete_point', response.result.point.id)
                        d.resolve(response.result.point)
            ).fail(
                (err) ->
                    console.log err
            )
            return d.promise
    }
    getters: {
        current_point: (state) ->
            return state.current_point

        route_for_edit: (state) ->
            return {
                'title': state.route.title,
                'description': state.route.description
            }

    }
})