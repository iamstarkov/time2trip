import Vue from 'vue'
import Router from 'vue-router'

import PointView from '../components/point/PointView.vue'
import EditRoute from '../components/route/EditRoute.vue'
import SubpointList from '../components/subpoint/SubpointList.vue'
import SubpointWizard from '../components/subpoint/SubpointWizard.vue'
import SubpointWizard__Foursquare from '../components/subpoint/SubpointWizard__Foursquare.vue'
import SubpointWizard__Custom from '../components/subpoint/SubpointWizard__Custom.vue'
import FoursquareVenueInfo from '../components/subpoint/FoursquareVenueInfo.vue'

import NotImplement from '../components/shared/NotImplement.vue'
import NotFound from '../components/shared/NotFound.vue'


Vue.use(Router)

let ShareView = NotImplement,
    PointInfo = NotImplement,
    NotesView = NotImplement,
    NoteEdit = NotImplement,
    SubpointInfo = NotImplement,
    SubpointEdit = NotImplement


// export default new Router({
//     linkActiveClass: 'active',
//     routes: [
//         { path: '/share', name: 'route.share', component: ShareView }, // map: all route
//         { path: '/edit', name: 'route.edit', component: EditRoute }, // map: all route
//         { path: '/points', name: 'points', component: PointView }, // map: all route
//         { path: '/points/:point_id', name: 'point.subpoints',  component: SubpointList }, // map: point + subpoints
        
//         { path: '/points/:point_id/info', name: 'point.info', component: PointInfo }, // map: point + subpoints
//         { path: '/points/:point_id/notes', name: 'point.notes', component: NotesView, // map: point + subpoints
//             props: (route) => ({type: 'point', id: route.params.point_id})
//         },
//         { path: '/points/:point_id/notes/:note_id/edit', name: 'point.note.edit', component: NoteEdit, props: true }, // map: point + subpoints
//         { path: '/points/:point_id/wizard', name: 'point.wizard', component: SubpointWizard, children: [
//             { path: '', redirect: 'foursquare/top'},
//             { path: 'foursquare/:category', name: 'point.wizard.foursquare', component: SubpointWizard__Foursquare, props: true},
//             { path: 'custom', name: 'point.wizard.custom', component: SubpointWizard__Custom },
//         ]},
//         { path: '/points/:point_id/:subpoint_id', name: 'point.subpoint.info', component: SubpointInfo },
//         { path: '/points/:point_id/:subpoint_id/edit', name: 'point.subpoint.edit', component: SubpointEdit },
//         { path: '/points/:point_id/:subpoint_id/notes', name: 'point.subpoint.notes', component: NotesView,
//             props: (route) => ({type: 'subpoint', id: route.params.subpoint})
//         },
//         { path: '/points/:point_id/:subpoint_id/notes/:note_id/edit', name: 'point.subpoint.note.edit', component: NoteEdit, props: true },

//         { path: '/venue/:venue_id', name: 'venue.info, component: FoursquareVenueInfo' },

//         { path: '/point/:point_id/wizard/venue/:venue_id', name: 'subpoint.wizard.venue', component: FoursquareVenueInfo },

//         { path: '', redirect: {name: 'points'}},
//         { path: '*', component: NotFound}
//     ]
// })
export default new Router({
    linkActiveClass: 'active',
    routes: [
        { path: '/share', name: 'route.share', component: ShareView }, // map: all route
        { path: '/edit', name: 'route.edit', component: EditRoute }, // map: all route
        { path: '/points', name: 'points', component: PointView }, // map: all route
        { path: '/points/:point_id', name: 'point.subpoints',  component: SubpointList }, // map: point + subpoints
        
        { path: '/points/:point_id/info', name: 'point.info', component: PointInfo }, // map: point + subpoints
        { path: '/points/:point_id/notes', name: 'point.notes', component: NotesView, // map: point + subpoints
            props: (route) => ({type: 'point', id: route.params.point_id})
        },
        { path: '/points/:point_id/notes/:note_id/edit', name: 'point.note.edit', component: NoteEdit, props: true }, // map: point + subpoints
        { path: '/points/:point_id/wizard', name: 'point.wizard', component: SubpointWizard, children: [
            { path: '', redirect: 'foursquare/top'},
            { path: 'foursquare/:category', name: 'point.wizard.foursquare', component: SubpointWizard__Foursquare, props: true},
            { path: 'custom', name: 'point.wizard.custom', component: SubpointWizard__Custom },
        ]},
        { path: '/points/:point_id/:subpoint_id', name: 'point.subpoint.info', component: SubpointInfo },
        { path: '/points/:point_id/:subpoint_id/edit', name: 'point.subpoint.edit', component: SubpointEdit },
        { path: '/points/:point_id/:subpoint_id/notes', name: 'point.subpoint.notes', component: NotesView,
            props: (route) => ({type: 'subpoint', id: route.params.subpoint})
        },
        { path: '/points/:point_id/:subpoint_id/notes/:note_id/edit', name: 'point.subpoint.note.edit', component: NoteEdit, props: true },

        { path: '/venue/:venue_id', name: 'venue.info, component: FoursquareVenueInfo' },

        { path: '/point/:point_id/wizard/venue/:venue_id', name: 'subpoint.wizard.venue', component: FoursquareVenueInfo },

        { path: '', redirect: {name: 'points'}},
        { path: '*', component: NotFound}
    ]
})









