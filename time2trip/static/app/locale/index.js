
export default {
  en: {
    shared: {
        add: 'Add',
        not_implement: 'Not implement, yet...'
    },
    add_point: {
      city: 'City'
    },
    points: {
        no_points: 'You don\'t have any points'
    },
    subpoints: {
        no_subpoints: 'You don\'t have any subpoints'
    }
  },
  ru: {
    shared: {
        add: 'Добавить',
        not_implement: 'В разработке'
    },
    add_point: {
      city: 'Город'
    },
    points: {
        no_points: 'Пока ничего нет'
    },
    subpoints: {
        no_subpoints: 'Пока ничего нет'
    }
  }
}