import Q from 'q'

export function make_marker(point, gmap, visible = false){
    return new google.maps.Marker({
        position: new google.maps.LatLng(point.lat, point.lng),
        map: gmap,
        title: point.title,
        visible: visible
    })
}

export function make_path (points, gmap, visible = false){
    let positions = points.map((p) => new google.maps.LatLng(p.lat, p.lng))
    return new google.maps.Polyline({
        path: positions,
        strokeColor: '#0000FF',
        strokeWeight: 5,
        geodesic: true,
        map: gmap,
        visible: visible
    })
}

export function coords_helper (coord) {
    let tmp = coord.slice(1, -1).replace(' ', '').split(',')
    return tmp.map((x) => parseFloat(x))
}

export function geocoder_search (options, lat = null, lng = null){
    let d = Q.defer(),
        geocoder = new google.maps.Geocoder()

    geocoder.geocode(
        options,
        (result, status) => {
            console.log(result, status)
            // console.log('status', google.maps.GeocoderStatus.OK)
            if (status != google.maps.GeocoderStatus.OK){
                d.reject(status)
            }
            else {
                let first_result = result[0]
                let obj = {
                    address: first_result.formatted_address
                }
                if (first_result.geometry.viewport){
                    obj.bounds = {
                        'southwest': coords_helper(first_result.geometry.viewport.getSouthWest().toString()),
                        'northeast':coords_helper(first_result.geometry.viewport.getNorthEast().toString())
                    }
                    obj.gmaps_bounds = first_result.geometry.viewport
                }
                if (lat == null & lng == null){
                    if (first_result.geometry.location){
                        lat = first_result.geometry.location.lat()
                        lng = first_result.geometry.location.lng()
                    }
                }
                obj.lat = lat
                obj.lng = lng
                d.resolve(obj)
            }

        }

    )
    return d.promise
}

export function search_by_coords (points){
    let pos = new google.maps.LatLng(lat, lng)
    let geocoder_options = {'latLng': pos}


}

function make_subpoint_markers (point){
    return
}

