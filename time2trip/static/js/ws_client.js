var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

window.WSClient = (function() {
  function WSClient(host, route_id, token, reconnected_clb) {
    this.host = host;
    this.route_id = route_id;
    this.token = token;
    this.reconnected_clb = reconnected_clb != null ? reconnected_clb : null;
    this.register_action = bind(this.register_action, this);
    this.request = bind(this.request, this);
    this.receive_handler = bind(this.receive_handler, this);
    this.on_close = bind(this.on_close, this);
    this.on_open = bind(this.on_open, this);
    this.create_connection = bind(this.create_connection, this);
    this.on_open_clb = bind(this.on_open_clb, this);
    this.url = "ws://" + this.host + "/?route_id=" + this.route_id + "&token=" + this.token;
    this.reconnecting_interval = 1000;
    this.request_id = 1;
    this.actions = {};
    this.promises = {};
    this.pending_requests = [];
    this.reconnecting = false;
    this.create_connection();
  }

  WSClient.prototype.on_open_clb = function() {};

  WSClient.prototype.create_connection = function() {
    this.connection = new WebSocket(this.url);
    this.connection.onopen = this.on_open;
    this.connection.onerror = this.error_handler;
    this.connection.onmessage = this.receive_handler;
    this.connection.onclose = this.on_close;
    return window.onbeforeunload = (function(_this) {
      return function() {
        return _this.connection.close();
      };
    })(this);
  };

  WSClient.prototype.on_open = function(evt) {
    var i, len, r, ref, results;
    console.log('Connection established');
    if (this.reconnecting) {
      if (this.reconnected_clb) {
        this.reconnected_clb('reconnected!!!');
      }
    }
    if (this.pending_requests.length) {
      ref = this.pending_requests;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        r = ref[i];
        console.log("[LOG] [WS_CLIENT] Handle pending requests", r);
        this.pending_requests.pop(r);
        results.push(this.connection.send(JSON.stringify(r)));
      }
      return results;
    }
  };

  WSClient.prototype.on_close = function(evt) {
    console.log('Connection was closed', evt);
    if (this.reconnected_clb && !this.reconnecting) {
      this.reconnected_clb('disconnected');
    }
    this.reconnecting = true;
    return setTimeout((function(_this) {
      return function() {
        console.log('Reconnecting...');
        return _this.create_connection();
      };
    })(this), this.reconnecting_interval);
  };

  WSClient.prototype.error_handler = function(err) {
    return console.log('WebSocket error!', err);
  };

  WSClient.prototype.receive_handler = function(evt) {
    var clb, data, i, len, ref, results;
    console.log('[LOG] [WS CLIENT] receive', evt);
    data = JSON.parse(evt.data);
    if (data.request_id) {
      if (this.promises[data.request_id] != null) {
        this.promises[data.request_id].resolve(data);
      }
    }
    if (data.action) {
      if (this.actions[data.action] != null) {
        ref = this.actions[data.action];
        results = [];
        for (i = 0, len = ref.length; i < len; i++) {
          clb = ref[i];
          results.push(clb(data));
        }
        return results;
      }
    }
  };

  WSClient.prototype.request = function(method, data) {
    var deferred, new_request_id, request_data;
    deferred = Q.defer();
    console.log('[LOG] [WS_CLIENT] request', method, data);
    new_request_id = this.request_id += 1;
    request_data = {
      'method': method,
      'data': data,
      'request_id': new_request_id
    };
    this.promises[new_request_id] = deferred;
    if (this.connection.readyState !== WebSocket.OPEN) {
      this.pending_requests.push(request_data);
    } else {
      this.connection.send(JSON.stringify(request_data));
    }
    return deferred.promise;
  };

  WSClient.prototype.register_action = function(name, clb) {
    console.log("Register action for " + name);
    if (this.actions[name] == null) {
      this.actions[name] = [];
    }
    return this.actions[name].push(clb);
  };

  return WSClient;

})();
