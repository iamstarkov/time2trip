# -*- coding:utf-8 -*-

import datetime

LOCALE_COOKIE_NAME = "_LOCALE_"
LOCALES = ('ru', 'en')


GOOGLE_PLACE_API = "https://maps.googleapis.com/maps/api/place/details/json?placeid={}&key={}&language={}"

POINT_NAME_CACHE_STORE = datetime.timedelta(days = 30)
FOURSQUARE_CACHE_STORE = datetime.timedelta(days = 30)


class UserRole (object):
    CREATOR = 'creator'
    MEMBER = 'member'
    VIEWER = 'viewer'


FOURSQUARE_CATEGORY = {
    "top": ('4bf58dd8d48988d163941735', '4bf58dd8d48988d164941735', '4bf58dd8d48988d165941735',
        '4bf58dd8d48988d190941735', '4bf58dd8d48988d18f941735', '4bf58dd8d48988d192941735',
        '4bf58dd8d48988d191941735','52e81612bcbc57f1066b7a25', '4bf58dd8d48988d166941735',
        '4bf58dd8d48988d15a941735', '56aa371be4b08b9a8d573547', '4deefb944765f83613cdba6e'
    ),
}
