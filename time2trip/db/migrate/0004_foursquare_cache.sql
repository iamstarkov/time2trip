CREATE TABLE foursquare_cache(
  key VARCHAR(255) NOT NULL PRIMARY KEY,
  value JSONB NOT NULL DEFAULT '{}',
  ctime timestamp with time zone not null DEFAULT now(),
  expire_at TIMESTAMP WITH TIME ZONE NOT NULL
);