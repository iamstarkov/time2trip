# -*- coding:utf-8 -*-

import json
import datetime

from logging import getLogger

from passlib.hash import sha256_crypt
from sqlalchemy.orm import relation
from sqlalchemy import func

from time2trip import constants

from .import Base, db_session, DeferredReflection

log = getLogger('time2trip')

class myBase(Base, DeferredReflection):
    __abstract__ = True
    __columns_include__ = []

    def fromDict(self, d):
        for k in d:
            setattr(self, k, d[k])

    def localize_col(self, column_name, lang):
        column_name = "{0}_{1}".format(column_name, lang)
        return getattr(self, column_name, None)

    def localize_attr(self, attr_name, locale):
        if locale in self.locale.get(attr_name, {}):
            return self.locale[attr_name][locale]
        else:
            return self.locale[self.locale.keys()[0]]

    @staticmethod
    def execute_sql(sql_exp):
        result = db_session.execute(sql_exp)
        return map(lambda d: dict(zip(result.keys(), d)), result.fetchall())

    @property
    def _dict(self):
        return dict((k, getattr(self, k)) for k in self.__columns_include__)

    def to_dict(self, *args):
        return self._dict

    @property
    def json(self):
        date_handler = lambda obj: (obj.isoformat() if isinstance(obj, (datetime.datetime, datetime.date)) else None)
        return json.dumps(self.to_dict(), default=date_handler)


class User(myBase):
    # __table__ = Table('users', meta, autoload=True)
    __tablename__ = 'users'
    __columns_include__ = ['id', 'name', 'email', 'locale']

    @property
    def locale(self):
        return self.options['language']

    def set_password(self, password):
        self.password = sha256_crypt.encrypt(password)

    def check_password(self, password):
        return sha256_crypt.verify(password, self.password)

class PointNameCache (myBase):
    __tablename__ = 'point_name_chache'

    @classmethod
    def get(cls, key):
        item = db_session.query(cls).filter(cls.gmap_id == key, cls.expire_at > func.timezone('UTC', func.now())).first()
        if item:
            log.debug('PointName cache hit: key = {}'.format(key))
            return item.value
        return None

    @classmethod
    def set(cls, key, value):
        db_session.query(cls).filter(cls.gmap_id == key, cls.expire_at <= func.timezone('UTC', func.now())).delete(synchronize_session=False)
        cache_item = PointNameCache()
        cache_item.gmap_id = key
        cache_item.value = value
        cache_item.expire_at = datetime.datetime.utcnow() + constants.POINT_NAME_CACHE_STORE
        log.debug('PointName cache write: key = {}'.format(key))
        db_session.add(cache_item)

class Route(myBase):
    # __table__ = Table('routes', meta, autoload=True)
    __tablename__ = 'routes'
    __columns_include__ = ['id', 'title', 'description', 'order']

    owner = relation('User', backref='routes')
    points_query = relation('Point', lazy='dynamic')

    @property
    def points (self):
        def sorting_func(k):
            return self.order.index(k) if k in self.order else len(self.order) + 1
        actual_points = self.points_query
        return sorted(actual_points, key=lambda k: sorting_func(k.id))

    def get_user_role (self, user):
        # if user.is_admin or self.user_id == user.id:
            # return 'creator'

        return 'creator'

    def to_dict (self):
        result = self._dict
        result['points'] = [point.to_dict() for point in self.points]
        return result

    def delete (self):
        for p in self.points:
            p.delete()

        db_session.delete(self)


class UserRoute(myBase):
    __tablename__ = 'users__routes'
    __columns_include__ = ['user_id', 'route_id', 'permission']

    user = relation('User')
    route = relation('Route')


class Point(myBase):
    __tablename__ = 'points'
    __columns_include__ = ['id', 'locale', 'lat', 'lng', 'country', 'bounds']

    route = relation('Route')
    subpoints_query = relation('Subpoint', order_by="Subpoint.id", lazy='dynamic')


    def to_dict (self):
        result = self._dict
        result['subpoints'] = [subpoint.to_dict() for subpoint in self.subpoints_query]
        return result

    def delete (self):
        for s in self.subpoints:
            s.delete()

        db_session.delete(self)


class Subpoint(myBase):
    __tablename__ = 'subpoints'
    __columns_include__ = ['id', 'source', 'title', 'note', 'type', 'lat', 'lng', 'address', 'bounds', 'options', 'creator_id']

    point = relation('Point', backref = 'subpoints')

    def delete (self):
        db_session.delete(self)


class FoursquareCache (myBase):
    __tablename__ = 'foursquare_cache'

    @classmethod
    def get(cls, key):
        item = db_session.query(cls).filter(cls.key == key, cls.expire_at > func.timezone('UTC', func.now())).first()
        if item:
            log.debug('Foursquare cache hit: key = {}'.format(key))
            return item.value
        return None

    @classmethod
    def set(cls, key, value):
        db_session.query(cls).filter(cls.key == key, cls.expire_at <= func.timezone('UTC', func.now())).delete(synchronize_session=False)
        cache_item = FoursquareCache()
        cache_item.key = key
        cache_item.value = value
        cache_item.expire_at = datetime.datetime.utcnow() + constants.POINT_NAME_CACHE_STORE
        log.debug('Foursquare cache write: key = {}'.format(key))
        db_session.add(cache_item)

